from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
import launch_ros.actions
import os
import yaml
from launch.substitutions import EnvironmentVariable
import pathlib
import launch.actions
from launch.actions import DeclareLaunchArgument
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    base_dir = get_package_share_directory("arduino_comm")
    param_dir = os.path.join(base_dir, "param")
    param_file_path = os.path.join(param_dir, "arduino.yaml")

    return LaunchDescription(
        [
            launch_ros.actions.Node(
                package="arduino_comm",
                executable="arduino_comm_node",
                name="arduino_comm_node",
                parameters=[
                    param_file_path
                ],
                remappings=[
                    ("ego_vehicle_control", "/roar/gokart/control"),
                    ("vehicle_status", "/roar/gokart/status"),
                ],
            ),
        ]
    )
